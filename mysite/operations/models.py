from django.db import models

# Create your models here.
from datetime import datetime


class Operation(models.Model):

    from_acc_no = models.UUIDField(editable=False)
    to_acc_no = models.UUIDField(editable=False)
    amount = models.BigIntegerField(default=0)
    sender_id = models.IntegerField()
    receiver_id = models.IntegerField()
    date_time = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return f"from_acc_no: {self.from_acc_no}, " \
               f"to_acc_no: {self.to_acc_no}, " \
               f"amount: {self.amount}" \
               f"sender_id: {self.sender_id}" \
               f"receiver_id: {self.receiver_id}" \
               f"date_time: {self.date_time}"

