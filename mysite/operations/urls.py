from django.urls import path

from operations import views

urlpatterns = [

    path('', views.get_operations),
    path('get-operations/<str:no>', views.get_operations_for_account)

]