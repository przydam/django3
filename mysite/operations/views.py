import json

from django.contrib.auth.models import User
from django.core import serializers
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from operations.models import Operation


@csrf_exempt
@require_http_methods("GET, POST")
def get_operations(request):
    operations = Operation.objects.all()
    print(operations)
    data = serializers.serialize('json', operations)
    # return HttpResponse("Hello, world. You're at the users index.")
    return HttpResponse(data, content_type="application/json")



@csrf_exempt
@require_http_methods("GET")
def get_operations_for_account(request, no):

    account_queryset = Operation.objects.filter(sender_id = no) | Operation.objects.filter(receiver_id = no)
    account_list = list(account_queryset)
    data = serializers.serialize('json',account_list)
    return HttpResponse(data, content_type="application/json")
