import uuid

from django.db import models


# Create your models here.


class Account(models.Model):
    no = models.UUIDField(editable=False, default=uuid.uuid4)
    user_id = models.IntegerField()
    balance = models.BigIntegerField(default=0)


    def __str__(self):
        return f"id: {self.id}, " \
               f"user_id: {self.user_id}, " \
               f"balance: {self.balance}"