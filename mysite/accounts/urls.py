from django.urls import path

from accounts import views

urlpatterns = [
    # path('', views.get_accounts, name='index'),
    path('', views.get_accounts),
    # path('/',views.index),
    path('create', views.create),
    path('deposit', views.deposit),
    path('transfer', views.transfer),
    #  path('get/<int:id>', views.get_users_by_id),
    #    path('delete/<int:id>', views.delete_user_by_id),
    #  path('update/<int:id>', views.update_user),
    #   path('activate/<int:id>', views.activate_user),
    #  path('deactivate/<int:id>', views.deactivate_user),
    #   path('change-password/<int:id>', views.change_password_user),

]
