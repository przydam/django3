import json
import json

from django.contrib.auth.models import User
from django.core import serializers
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

# from accounts.models import Account
from accounts.models import Account
from operations.models import Operation


@csrf_exempt
@require_http_methods("GET, POST")
def get_accounts(request):
    accounts = Account.objects.all()
    print(accounts)
    data = serializers.serialize('json', accounts)
    # return HttpResponse("Hello, world. You're at the users index.")
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
@require_http_methods("POST")
def create(request):
    body = request.body.decode("utf-8")
    json_body = json.loads(body)
    user_id = json_body["userId"]
    account = Account(user_id=user_id)
    account.save()
    return HttpResponse("Utworzono konto", status=201)


@csrf_exempt
@require_http_methods("POST")
def deposit(request):
    body = request.body.decode("UTF-8")
    json_body = json.loads(body)
    user_id = json_body["userId"]
    amount = json_body["amount"]
    account_no = json_body["accountNO"]
    account_queryset = Account.objects.filter(no=account_no, user_id=user_id)

    account_list = list(account_queryset)

    if len(account_list) == 0:
        return HttpResponse("nie znaleziono konta", status=404)

    account = account_list[0]

    account.balance += amount
    account.save()
    return HttpResponse("Zaktualizowano saldo", status=200)


@csrf_exempt
@require_http_methods("POST")
def transfer(request):
    body = request.body.decode("UTF-8")
    json_body = json.loads(body)
    sender_acc = json_body["senderACC"]
    receiver_acc = json_body["receiverACC"]
    amount = json_body["amount"]
    account_sender_acc = Account.objects.filter(no=sender_acc)
    account_receiver_acc = Account.objects.filter(no=receiver_acc)

    account_list_sender = list(account_sender_acc)
    account_list_receiver = list(account_receiver_acc)

    if len(account_list_sender) == 0:
        return HttpResponse("Niepoprawny nymer konta", status=404)
    if len(account_list_receiver) == 0:
        return HttpResponse("nie znaleziono konta na które miały zostać wysłane pieniądze", status=404)

    sender = account_list_sender[0]
    receiver = account_list_receiver[0]

    if sender.balance <= amount:
        return HttpResponse("Brak wystarczających srodkow", status=418)

    sender.balance -= amount
    receiver.balance += amount
    sender.save()
    receiver.save()

    sender_id = sender.user_id
    receiver_id = receiver.user_id

    operation = Operation(from_acc_no=sender_acc,to_acc_no =receiver_acc,amount=amount,sender_id=sender_id,receiver_id= receiver_id)
    operation.save()
    return HttpResponse("Przetransferowano srodki", status=200)
