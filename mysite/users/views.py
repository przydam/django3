import json

from django.contrib.auth.models import User
from django.core import serializers
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods


@csrf_exempt
@require_http_methods("GET, POST")
def get_users(request):
    users = User.objects.all()
    print(users)
    data = serializers.serialize('json', users)
    # return HttpResponse("Hello, world. You're at the users index.")
    return HttpResponse(data, content_type="application/json")


@csrf_exempt
@require_http_methods("POST, GET")
def create_user(request):
    body = request.body.decode("UTF-8")
    # print(body)
    json_body = json.loads(body)
    try:
        email = json_body["email"]
    except:
        return HttpResponse("Email not provided", status=409)
    username = json_body["username"]
    first_name = json_body["firstName"]
    last_name = json_body["lastName"]
    age = json_body["age"]
    password = json_body["password"]
    id = json_body["id"]

    user = User()
    user.id = id
    user.email = email
    user.first_name = first_name
    user.username = username
    user.last_name = last_name
    user.age = age
    user.password = password
    user.active = True
    user.save()

    return HttpResponse("Dodano użytkownika")


@csrf_exempt
@require_http_methods("GET")
def get_users_by_id(request, id):
    users = User.objects.filter(id=int(id))
    data = serializers.serialize('json', users)
    return HttpResponse(data, content_type="application/json")

    return HttpResponse(users)


@csrf_exempt
@require_http_methods("DELETE")
def delete_user_by_id(request, id):
    users_to_delete = User.objects.filter(id=int(id))
    if len(list(users_to_delete)) == 0:
        return HttpResponse("Nie ma takiego użytkownika", status=404)
    else:
        users_to_delete.delete()
        return HttpResponse("Usunięto użytkownika", status=204)


@csrf_exempt
@require_http_methods("PATCH")
def update_user(request, id):
    user_body = json.loads(request.body.decode("UTF_8"))

    first_name = user_body["firstName"]
    last_name = user_body["lastName"]
    age = user_body["age"]
    users = User.objects.filter(id=id)

    if len(list(users)) == 0:
        return HttpResponse("Nie ma takiego użytkownika", status=404)
    else:
        user = users[0]
        user.first_name = first_name
        user.last_name = last_name
        user.age = age
        user.save()
        return HttpResponse("Zaktualizowanao użytkownika o id: " + str(id), status=200)


@csrf_exempt
@require_http_methods("PATCH")
def activate_user(request, id):
    users = User.objects.filter(id=id)
    if len(list(users)) == 0:
        return HttpResponse("Nie ma takiego użytkownika", status=404)
    else:
        user = users[0]
        user.is_active = True
        user.save()
        return HttpResponse("Aktywowano użytkownika o id: " + str(id), status=200)


@csrf_exempt
@require_http_methods("PATCH")
def deactivate_user(request, id):
    users = User.objects.filter(id=id)
    if len(list(users)) == 0:
        return HttpResponse("Nie ma takiego użytkownika", status=404)
    else:
        user = users[0]
        user.is_active = False
        user.save()
        return HttpResponse("Zdezaktywowano użytkownika o id: " + str(id), status=200)


@csrf_exempt
@require_http_methods("PATCH")
def change_password_user(request, id):
    user_body = json.loads(request.body.decode("UTF_8"))
    users = User.objects.filter(id=id)
    new_password = user_body["newPassword"]
    old_password = user_body["oldPassword"]
    user = users[0]
    if new_password == user.password:
        return HttpResponse("Nowe hasło nie różni się od starego", status=404)

    if len(list(users)) == 0:
        return HttpResponse("Nie ma takiego użytkownika", status=404)
    else:

        if old_password == user.password:
            user.password = new_password
            user.save()
            return HttpResponse("Zaktualizowanao hasło użytkownika o id: " + str(id), status=200)
        else:
            return HttpResponse("Niepoprawne stare hasło", status=404)
