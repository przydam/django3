from django.urls import path

from . import views

urlpatterns = [
    path('', views.get_users, name='index'),
    # path('/',views.index),
    path('create', views.create_user),
    path('get/<int:id>', views.get_users_by_id),
    path('delete/<int:id>', views.delete_user_by_id),
    path('update/<int:id>', views.update_user),
    path('activate/<int:id>', views.activate_user),
    path('deactivate/<int:id>', views.deactivate_user),
    path('change-password/<int:id>', views.change_password_user),

]
